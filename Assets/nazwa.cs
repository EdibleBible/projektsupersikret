using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nazwa : MonoBehaviour
{
    public int spid;
    void Start()
    {
    }

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.A))
        {
            transform.position = transform.position + (Vector3.left * spid * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position = transform.position + (Vector3.right * spid * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position = transform.position + (Vector3.up * spid * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position = transform.position + (Vector3.down * spid * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.position = transform.position + (Vector3.back * spid * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.position = transform.position + (Vector3.forward * spid * Time.deltaTime);
        }
    }
}
